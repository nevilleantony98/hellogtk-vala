public class App.Reminder : Gtk.Application{

        public Reminder () {
                Object(application_id: "com.nevilleantony98.Reminder",
                flags: ApplicationFlags.FLAGS_NONE);
        }

        protected override void activate () {
                var window = new ApplicationWindow ();
                window.show_all ();
        }

        public static int main(string[] args){
                var app = new Reminder ();
                return app.run(args);
        }
}